# SCRIPT VERSION => 1.3

# Require librairies!
use strict;
use warnings;
use Data::Dumper;
use DBI;
use JSON;
use File::Copy;
use File::Path 'rmtree';

# Nimsoft
use lib "D:/apps/Nimsoft/perllib";
use lib "D:/apps/Nimsoft/Perl64/lib/Win32API";
use Nimbus::API;
use Nimbus::CFG;
use Nimbus::PDS;
use Socket;

# librairies
use bnpp::main;
use bnpp::cmdb;
use bnpp::alarms;
use bnpp::nimdate;
use bnpp::ump;

#
# Load configuration !
#
my $LOG;
my $GBL_ET = time();

sub dateLog {
	my @months = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
	my @days = qw(Sun Mon Tue Wed Thu Fri Sat Sun);
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	return "$months[$mon] $mday $hour:$min:$sec";
}

sub Log {
    my $msg = shift;
    my $date = dateLog();
    print $LOG "$date - $msg\n";
    print "$msg\n";
}

unless(open($LOG,">","alarms_management.log")) {
    die("Unable to open the log file!");
}

Log("########################################");
Log("### Execution start at ".localtime()." ###");
Log("########################################");

my $CFG = Nimbus::CFG->new("alarms_management.cfg");
my @ARR_UMP         = split(",",$CFG->{"ump"}->{"servers"});
my $UMP_Pool        = new bnpp::ump(@ARR_UMP);
my $Alarm_maxtime   = $CFG->{"setup"}->{"maxtime"} || 2678400; # Default 31 day
my $AuditMode       = $CFG->{"setup"}->{"audit"} || 0; # 0 == no audit mode
my $Cache_day       = $CFG->{"setup"}->{"cache_day"} || 3;

# Retrive CFG value and assign fallback if no value provided!
my $alarms_origin 			= $CFG->{"alarms"}->{"origin"} || 0;
my $alarms_usertag2 		= $CFG->{"alarms"}->{"usertag2"} || 0;
my $alarms_inactive 		= $CFG->{"alarms"}->{"inactive"} || 0;
my $alarms_probedown 		= $CFG->{"alarms"}->{"probe_down"} || 0;
my $alarms_time 			= $CFG->{"alarms"}->{"time"} || 1;
my $alarms_clear_assetdb 	= $CFG->{"alarms"}->{"clear_not_in_assetdb"} || 0;
my $alarms_clear_ip 		= $CFG->{"alarms"}->{"clear_ip"} || 0;

if(not $UMP_Pool->isConnected()) {
    Log("Unable to connect on one of the configuration ump in the pool!");
    close $LOG;
    exit(1);
}
else {
    Log("Selected Active UMP is => $UMP_Pool->{active}");
}

# ************************************************* #
# DBI
# ************************************************* #
my %CMDB_Hash;
{
    my $DB_User         = $CFG->{"CMDB"}->{"sql_user"};
    my $DB_Password     = $CFG->{"CMDB"}->{"sql_password"};
    my $DB_SQLServer    = $CFG->{"CMDB"}->{"sql_host"};
    my $DB_Database     = $CFG->{"CMDB"}->{"sql_database"};

    my $DB = DBI->connect("$DB_SQLServer;UID=$DB_User;PWD=$DB_Password",{
        RaiseError => 1,
        AutoCommit => 0
    });

    if(not defined($DB)) {
        Log("Failed to contact database!");
        close $LOG;
        exit(1);
    }
    else {
        Log("Contact database successfull! Connection info : $DB_Database");
        $DB->do("USE $DB_Database");
    }

    # Get hash of assetDB hostname and UT2 /
    %CMDB_Hash = bnpp::cmdb::getLIST($DB);
    Log("Get AssetDB rows successfully!");

    $DB->disconnect();
}

my $SDK;
Log("Instanciating bnpp framework and set UMP authentifcation!");
$SDK = new bnpp::main("alarms_management",$CFG->{"setup"}->{"domain"},0);
$SDK->setAuthentification($CFG->{"ump"}->{"user"},$CFG->{"ump"}->{"password"});
Log("Create output directory.");
my $output_date = $SDK->getDate();
$SDK->createDirectory("output/$output_date");

Log("Starting cleanup of output directories");
{
    my $exec_annee  = substr($output_date,0,4);
    my $exec_month  = substr($output_date,4,2);
    my $exec_day    = substr($output_date,6,2);
    my $regular_day = 31;

    my @removeDirectory = ();
    opendir(DIR,'output/');
    my @directory = readdir(DIR);
    foreach(@directory) {
        next if $_ eq '.' || $_ eq '..';
        my $annee   = substr($_,0,4);
        my $month   = substr($_,4,2);
        my $day     = substr($_,6,2);

        if($exec_annee > $annee) {
            push(@removeDirectory,$_);
            next;
        }
        else {
            my $calc = $exec_day - $Cache_day;
            if( $calc <= 0 ) {
                my $average_day = $regular_day - abs($calc);
                if( $average_day > $day && $month != $exec_month ) {
                    push(@removeDirectory,$_);
                    next;
                }
            }
            elsif( $calc > $day ) {
                push(@removeDirectory,$_);
                next;
            }
        }
    }

	if(scalar(@removeDirectory) > 0) {
		foreach(@removeDirectory) {
			Log("Remove old output directory with name $_");
			rmtree("output/$_");
		}
	}
	else {
		Log("No old directories to remove!");
	}
}

#
# Get all alarms
#
my $HTTP_Res = $SDK->HTTP("GET","$UMP_Pool->{active}/rest/alarms");
if($HTTP_Res->{"_rc"} == 200) {
    Log("Getting alarms from UMP RESTService OK!");

    Log("Parsing and saving alarms.");
    my $HASH_UMP_Alarms = JSON->new->utf8(1)->decode( $HTTP_Res->decoded_content );
    my @ARR_Alarms = ();
    foreach my $alarm ( @{ $HASH_UMP_Alarms->{"alarm"} } ) {
        push(@ARR_Alarms,new bnpp::alarms($alarm));
    }
    undef $HASH_UMP_Alarms;
    undef $HTTP_Res;

    my %CleanStats = (
        usertag2 => 0,
		rejected_usertag2 => 0,
		rejected_origin => 0,
        dead => 0,
        fail_restart => 0,
        inactive_robot => 0,
		origin => 0,
		exclude_pool => 0,
		not_in_assetdb => 0,
		resolve_ip_fail => 0,
		specific => 0,
		invalid_hostname => 0
    );

    my $Acknowledge = 0;
    my %AcknowledgeID = ();

    sub FN_Acknowledge {
        my ($msg,$var) = @_;
        Log($msg);
        $CleanStats{$var}++;
        $Acknowledge = 1;
    }

	my $Alarms_count = scalar @ARR_Alarms;
    Log("Detecting alarms which have to be Acknowledge, number of alarms => $Alarms_count");
    foreach (@ARR_Alarms) {

		my $robotname = $_->{hostname};
		my $resolve = 1;
		$Acknowledge = 0;
		if(not defined($robotname)) {
			Log("Hostname for $_->{hostname} is not defined, go the next alarm!");
			next;
		}

		# Detect if robotname is ip
		if($robotname =~ /^\d+.\d+.\d+.\d+$/) {
			my $ip = $robotname;
			if($ip ne "127.0.0.1" || index($ip, "169.254") == -1) {
				if($alarms_clear_ip eq "1" ) {
					FN_Acknowledge("Failed to resolve ip => $ip but force acknowledge!","resolve_ip_fail");
					$resolve = 0;
				}
				else {
					Log("Failed to resolve ip => $ip");
					$CleanStats{resolve_ip_fail}++;
					next;
				}
			}
			else {
				next;
			}
		}

		if($resolve) {
			if(index($_->{hostname}, "net.intra") != -1 || index($_->{hostname}, ".fr") != -1) {
				($robotname) = ($_->{hostname} =~ /^([^\.]+)/);
				Log("Transform robot name $_->{hostname} to $robotname!");
			}

			# Exclude localhost servers
			if($robotname eq "localhost") {
				next;
			}
		}


		my $nimDate = new bnpp::nimdate($_->{timeLast});
		Log("[$robotname] Processing AlarmID $_->{id} - date : $_->{timeLast} with a difference of ".abs($nimDate->{diff})." seconds");

        if(exists($AcknowledgeID{$_->{id}})) {
			Log("Already acknowledge, next() executed!");
            next;
        }

        #
        # Date difference
        #
        if($alarms_time eq "1" && $_->{probe} ne "logmon" && $_->{severity} ne "information" && not $Acknowledge) {
            FN_Acknowledge("Acknowledge dead alarm in time $_->{id} , last occurence => $_->{timeLast}","dead") if abs($nimDate->{diff}) >= $Alarm_maxtime;
        }

        #
        # Robot is inactive!
        #
        if($alarms_inactive eq "1" && not $Acknowledge) {
            if(index($_->{message}, "is inactive") != -1) {
                my $PDS = pdsCreate();
            	my ($RC, $O) = nimNamedRequest("/$_->{domain}/$_->{hub}/$_->{robot}", "get_info", $PDS,5);
            	pdsDelete($PDS);
                FN_Acknowledge("Acknowledge robot is inactive $_->{id} , the robot is now active!","inactive_robot") if $RC == NIME_OK;
            }
        }

		if($_->{probe} eq "hubmon" || $_->{probe} eq "ibmvm" || $_->{probe} eq "vmware") {
			Log("Exclude alarm from pool processing because probe equal => $_->{probe}");
			next;
		}

		#
		# We check if the robot exist on the AssetDB table!
		#
		if($alarms_usertag2 eq "1" || $alarms_origin eq "1" && not $Acknowledge) {
			if(defined($CMDB_Hash{$robotname})) {

				#
				# Alarms usertag2 <=> Asset_DB Usertag2
				#
				if($alarms_usertag2 && index($_->{message}, "HMC") == -1) {
					my $usertag2 = $CMDB_Hash{$robotname}->{usertag2};
					if(defined($usertag2)) {
						if($_->{userTag2} ne "ENRICHMENT FAILED" && $usertag2 ne $_->{userTag2}) {
							FN_Acknowledge("Acknowledge bad usertag2 $_->{id}, alarm os_user2 : $_->{userTag2} <> assetdb : $usertag2","usertag2");
							Log("Focus alarm => $_->{message}");
						}
					}
					else {
						if($CFG->{"alarms"}->{"clear_not_in_assetdb"} eq "1") {
							FN_Acknowledge("Alarm $_->{id} for $robotname have no origin on the AssetDB table.","rejected_usertag2");
						}
						else {
							Log("Alarm $_->{id} for $robotname have no origin on the AssetDB table.");
							$CleanStats{rejected_usertag2}++;
						}
					}
				}

				if($alarms_origin eq "1" && not $Acknowledge) {
					my $origin = $CMDB_Hash{$robotname}->{origin};
					if(defined($origin)) {
						my $focus_origin = $_->originExist($origin);
						if($focus_origin ne "") {
							my @ARR = $_->{origin_arr};
							FN_Acknowledge("Acknowledge bad origin $_->{id}, alarm origin : @ARR <> assetdb : $origin","origin");
						}
					}
					else {
						if($CFG->{"alarms"}->{"clear_not_in_assetdb"} eq "1") {
							FN_Acknowledge("Alarm $_->{id} for $robotname have no usertag2 on the AssetDB table.","rejected_origin");
						}
						else {
							Log("Alarm $_->{id} for $robotname have no usertag2 on the AssetDB table.");
							$CleanStats{rejected_origin}++;
						}
					}
				}

			}
			else {
				if($robotname ne "ERROR" && $robotname ne "1m" && $robotname ne "WAS" && $robotname ne "IHS") {
					if($alarms_clear_assetdb eq "1") {
						FN_Acknowledge("Alarm $_->{id} for $robotname is not in the AssetDB table.","exclude_pool");
					}
					else {
						Log("$robotname is not in the AssetDB table for Alarm $_->{id}");
						$CleanStats{exclude_pool}++;
					}

					if($_->{userTag2} eq "NOT IN ASSETDB") {
						$CleanStats{not_in_assetdb}++;
					}
					else {
						Log("[$_->{userTag2}] Specific alarm usertag2 => $_->{message}");
						$CleanStats{specific}++;
					}
				}
				else {
					Log("Alarm $_->{id} find with a hostname equal to ERROR !");
					Log("Message => $_->{message}");
					$CleanStats{invalid_hostname}++;
				}
			}
		}

		#
		# Probe failed to start!
		#
		if($alarms_probedown eq "1" && not $Acknowledge) {
			if (index($_->{message}, "FAILED to start") != -1) {
				my $HTTP_RobotNFO = $SDK->HTTP("GET","$UMP_Pool->{active}/rest/hubs/$_->{domain}/$_->{hub}/$_->{robot}");
				if($HTTP_RobotNFO->{"_rc"} == 200) {
					my $HASH_Robots = JSON->new->utf8(1)->decode( $HTTP_RobotNFO->decoded_content );
					foreach my $probe ( @{ $HASH_Robots->{"probes"} } ) {
						if($probe->{command} eq "$_->{suppressionKey}.exe" && $probe->{active} eq "true") {
							FN_Acknowledge("Acknowledge probe failed to start $_->{id}, probe $_->{suppressionKey} on robot $_->{robot}","fail_restart");
							last;
						}
					}
				}
			}
		}


        if($Acknowledge) {
            $AcknowledgeID{$_->{id}} = 1;
        }
    }
    undef $Acknowledge;
    undef $Alarm_maxtime;
    undef @ARR_Alarms;

    #
    # Stats echo...
    #
	Log("");
    Log("Finals stats :");
    foreach my $StatsKey (keys %CleanStats) {
        Log("$StatsKey => $CleanStats{$StatsKey}");
    }
    undef %CleanStats;
	Log("");
	Log("");

    #
    # Acknowledge alarms
    #
    my $Acknowledge_count = 1;
    my $max = keys %AcknowledgeID;
	my %FailedAcknowLedge = ();
    if($max > 0) {
        Log("Starting acknowledge in bulk, number of acknowledge to do => $max");
        if(not $AuditMode) {
            foreach my $alarmid (keys %AcknowledgeID) {
                Log("Acknowledge now -> $alarmid [count $Acknowledge_count / $max]");
                $Acknowledge_count++;
                my $HTTP_Acknowledge = $SDK->PUT("$UMP_Pool->{active}/rest/alarms/$alarmid/ack");
                if($HTTP_Acknowledge->{"_rc"} != 204) {
                    Log("Fail to acknowledge -> $alarmid");
					$FailedAcknowLedge{$alarmid} = 1;
                }
            }

			# Failed acknowledge
			Log("\nRetry acknowledge for failed http request! \n");
			foreach my $alarmid (keys %FailedAcknowLedge) {
                Log("Acknowledge failed -> $alarmid");
                my $HTTP_Acknowledge = $SDK->PUT("$UMP_Pool->{active}/rest/alarms/$alarmid/ack");
                if($HTTP_Acknowledge->{"_rc"} != 204) {
                    Log("Fail to acknowledge -> $alarmid");
                }
            }
        }
        else {
            Log("No acknowledge action because AuditMode is activated!");
        }
    }
    else {
        Log("No alarms to acknowledge, terminating the script...");
    }

}
else {
    Log("Failed to get alarms list!");
}

#
# Echo script execution time!
#
{
    my $FINAL_TIME  = sprintf("%.2f", time() - $GBL_ET);
    my $Minute      = sprintf("%.2f", $FINAL_TIME / 60);
    Log("Final execution time = $FINAL_TIME second(s) [$Minute minutes]!");
}


Log("Copy log file into output/$output_date directory!");
copy("alarms_management.log","output/$output_date/alarms_management.log") or warn "Failed to copy the log into the final outdir";

Log("########################################");
Log("### Execution end at ".localtime()." ###");
Log("########################################");


close $LOG;
1;
