package bnpp::cmdb;
use strict;
use warnings;
use Exporter qw(import);
use DBI;

use Data::Dumper;
$Data::Dumper::Indent = 1;

our @EXPORT_OK = qw(getLIST);

sub getLIST {
    my ($DB) = @_;
    my %ARR_CMDB = ();
    my $STH = $DB->prepare("SELECT HOSTNAME,PILOTAGE_OLD,CENTRE_ORGA FROM NIMSOFT_AssetDB");
    $STH->execute();
    my( $HOSTNAME, $PILOTAGE_OLD, $CENTRE_ORGA );
    $STH->bind_columns( undef, \$HOSTNAME, \$PILOTAGE_OLD, \$CENTRE_ORGA );
    while($STH->fetch()) {
        if(defined($HOSTNAME)) {
            $ARR_CMDB{lc $HOSTNAME} = {
                usertag2 => $PILOTAGE_OLD || "",
                origin => $CENTRE_ORGA || ""
            };
        }
    }
    $STH->finish;
    return %ARR_CMDB;
}

1;
