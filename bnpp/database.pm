use strict;
use warnings;

# Namespace
package bnpp::database;

# Nimsoft librairies !
use lib "D:/apps/Nimsoft/perllib";
use lib "D:/apps/Nimsoft/Perl64/lib/Win32API";
use Nimbus::API;
use Nimbus::PDS;
use Nimbus::CFG;

use Term::ANSIColor qw(:constants);
use Win32::Console::ANSI;

use Data::Dumper;
$Data::Dumper::Indent = 1;

sub new {
    my ($class,$SQLServer,$User,$Password,$Database) = @_;
    my $this = {
        ctx => DBI->connect("$SQLServer;UID=$User;PWD=$Password",{RaiseError => 1,AutoCommit => 1}),
        ctx2 => DBI->connect("$SQLServer;UID=$User;PWD=$Password",{RaiseError => 1,AutoCommit => 1}),
        hub => undef,
        database => $Database
    };
    return bless($this,ref($class) || $class);
}

sub closePool {
    my ($this) = @_;
    $this->{ctx}->disconnect;
    $this->{ctx2}->disconnect;
}

sub defined {
    my ($self) = @_;
    my $DB      = $self->{ctx};
    my $DB2     = $self->{ctx2};
    if(defined($DB) && defined($DB2)) {
        $DB->do("USE $self->{database}");
        $DB2->do("USE $self->{database}");
        return 1;
    }
    return 0;
}

sub setHUB {
    my ($self,$hub) = @_;
    my $DB = $self->{ctx};
    my $DB2 = $self->{ctx2};

    my $sth = $DB->prepare("SELECT * FROM list_hubs WHERE hubname = ?");
    $sth->execute($hub->{name});

    if($sth->rows) {
        my @HUBArray;
        while(@HUBArray = $sth->fetchrow_array) {
            my $id          = $HUBArray[0];
            my $sth_update = $DB2->prepare("UPDATE list_hubs SET hubname=?,domain=?,version=?,hubaddr=?,hubip=?,origin=?,ldap=?,uptime=?,started=?,tunnel=? WHERE hubname = ?");
            $sth_update->execute(
                $hub->{name},
                $hub->{domain},
                $hub->{version},
                $hub->{addr},
                $hub->{ip},
                $hub->{origin},
                $hub->{ldap},
                $hub->{uptime},
                $hub->{started},
                $hub->{tunnel},
                $hub->{name}
            ) or return 0;
            $sth_update->finish();

            $self->{fk} = $id;
            last;
        }
    }
    else {
        my $sth_insert = $DB2->prepare("INSERT INTO list_hubs (hubname,domain,version,hubaddr,hubip,origin,ldap,uptime,started,tunnel) VALUES (?,?,?,?,?,?,?,?,?,?)");
        $sth_insert->execute(
            $hub->{name},
            $hub->{domain},
            $hub->{version},
            $hub->{addr},
            $hub->{ip},
            $hub->{origin},
            $hub->{ldap},
            $hub->{uptime},
            $hub->{started},
            $hub->{tunnel}
        ) or return 0;
        $sth_insert->finish();
        my @rowID = $DB2->selectrow_array('SELECT @@IDENTITY');
        my $ID = $rowID[0];
        print "$ID \n";
        $self->{fk} = $ID;
    }

    $self->{hub} = $hub;
    return 1;
}

sub setSecond {
    my ($self,$robot) = @_;
    my $DB  = $self->{ctx};
    my $DB2 = $self->{ctx2};

    ### PRIMARY
    my $sth_primary = $DB->prepare("SELECT * FROM list_hubs_primary WHERE robotid = ?");
    $sth_primary->execute($robot->{self_fk});

    if($sth_primary->rows) {
        my $primary_update = $DB2->prepare("UPDATE list_hubs_primary SET domain=?,name=?,robotname=?,ip=?,dns_name=?,port=? WHERE robotid=?");
        $primary_update->execute(
            $robot->{phub_domain},
            $robot->{phub_name},
            $robot->{phub_robotname},
            $robot->{phub_ip},
            $robot->{phub_dns_name},
            $robot->{phub_port},
            $robot->{self_fk}
        );
        $primary_update->finish();
    }
    else {
        my $primary_insert = $DB2->prepare("INSERT INTO list_hubs_primary (robotid,domain,name,robotname,ip,dns_name,port) VALUES (?,?,?,?,?,?,?)");
        $primary_insert->execute(
            $robot->{self_fk},
            $robot->{phub_domain},
            $robot->{phub_name},
            $robot->{phub_robotname},
            $robot->{phub_ip},
            $robot->{phub_dns_name},
            $robot->{phub_port},
        );
        $primary_insert->finish();
    }
    $sth_primary->finish();

    ### SECONDARY
    my $sth_secondary = $DB->prepare("SELECT * FROM list_hubs_secondary WHERE robotid = ?");
    $sth_secondary->execute($robot->{self_fk});

    if($sth_secondary->rows) {
        my $secondary_update = $DB2->prepare("UPDATE list_hubs_secondary SET domain=?,name=?,robotname=?,ip=?,dns_name=?,port=? WHERE robotid=?");
        $secondary_update->execute(
            $robot->{shub_domain},
            $robot->{shub_name},
            $robot->{shub_robotname},
            $robot->{shub_ip},
            $robot->{shub_dns_name},
            $robot->{shub_port},
            $robot->{self_fk}
        );
        $secondary_update->finish();
    }
    else {
        my $secondary_insert = $DB2->prepare("INSERT INTO list_hubs_secondary (robotid,domain,name,robotname,ip,dns_name,port) VALUES (?,?,?,?,?,?,?)");
        $secondary_insert->execute(
            $robot->{self_fk},
            $robot->{shub_domain},
            $robot->{shub_name},
            $robot->{shub_robotname},
            $robot->{shub_ip},
            $robot->{shub_dns_name},
            $robot->{shub_port}
        );
        $secondary_insert->finish();
    }
    $sth_secondary->finish();

    return 1;
}

sub setROBOT {
    my ($self,$robot) = @_;
    my $DB = $self->{ctx};
    my $DB2 = $self->{ctx2};

    my $sth = $DB->prepare("SELECT * FROM list_robots WHERE robotname = ?");
    $sth->execute($robot->{name});

    if($sth->rows) {
        my @ROBOTArray;
        my $ID;
        while(@ROBOTArray = $sth->fetchrow_array) {
            my $robot_update = $DB2->prepare("UPDATE list_robots SET hubid=?,robotname=?,robotip=?,domain=?,origin=?,source=?,robot_device_id=?,uptime=?,started=?,os_major=?,os_minor=?,os_version=?,os_description=?,os_user1=?,os_user2=?,status=? WHERE robotname = ?");
            $robot_update->execute(
                $self->{fk},
                $robot->{name},
                $robot->{ip},
                $robot->{domain},
                $robot->{origin},
                $robot->{source},
                $robot->{device_id},
                $robot->{uptime},
                $robot->{started},
                $robot->{os_major},
                $robot->{os_minor},
                $robot->{os_version},
                $robot->{os_description},
                $robot->{os_user1},
                $robot->{os_user2},
                $robot->{status},
                $robot->{name}
            );
            $robot_update->finish();

            $ID = $ROBOTArray[0];
            last;
        }
        return $ID;
    }
    else {
        my $robot_insert = $DB2->prepare("INSERT INTO list_robots (hubid,robotname,robotip,domain,origin,source,robot_device_id,uptime,started,os_major,os_minor,os_version,os_description,os_user1,os_user2,status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $robot_insert->execute(
            $self->{fk},
            $robot->{name},
            $robot->{ip},
            $robot->{domain},
            $robot->{origin},
            $robot->{source},
            $robot->{device_id},
            $robot->{uptime},
            $robot->{started},
            $robot->{os_major},
            $robot->{os_minor},
            $robot->{os_version},
            $robot->{os_description},
            $robot->{os_user1},
            $robot->{os_user2},
            $robot->{status}
        );
        $robot_insert->finish();

        my @rowID = $DB2->selectrow_array('SELECT @@IDENTITY');
        return $rowID[0];
    }

    return undef;
}

sub robotSUCCESS {
    my ($self,$RobotFK,$RC) = @_;
    my $DB = $self->{ctx};
    my $sth = $DB->do("UPDATE list_robots SET success = ".$RC." WHERE id = '".$RobotFK."' ");
}

sub probeSUCCESS {
    my ($self,$ProbeFK,$RC) = @_;
    my $DB = $self->{ctx};
    my $sth = $DB->do("UPDATE list_probes SET success = ".$RC." WHERE id = '".$ProbeFK."'");
}

sub setPROBE {
    my ($self,$probe,$robotid) = @_;
    my $DB = $self->{ctx};
    my $DB2 = $self->{ctx2};

    my $sth = $DB->prepare("SELECT * FROM list_probes WHERE robotid = ? AND name = ?");
    $sth->execute($robotid,$probe->{name});

    if($sth->rows) {
        my @ProbeArr;
        while(@ProbeArr = $sth->fetchrow_array) {
            my $probe_update = $DB2->prepare("UPDATE list_probes SET robotid=?,name=?,pid=?,active=?,command=?,arguments=?,timespec=?,times_activated=?,times_started=?,last_action=?,last_started=?,process_state=?,pkg_name=?,pkg_version=?,pkg_build=? WHERE name = ?")  or die "prepare err: ", $DB2->errstr;
            $probe_update->execute(
                $robotid,
                $probe->{name},
                $probe->{pid},
                $probe->{active},
                $probe->{command},
                $probe->{arguments},
                $probe->{timespec},
                $probe->{times_activated},
                $probe->{times_started},
                $probe->{last_action},
                $probe->{last_started},
                $probe->{process_state},
                $probe->{pkg_name},
                $probe->{pkg_version},
                $probe->{pkg_build},
                $probe->{name}
            ) or die "err: ", $DB2->errstr;
            $probe_update->finish();

            return $ProbeArr[0];
        }
    }
    else {
        my $probe_insert = $DB2->prepare("INSERT INTO list_probes (robotid,name,pid,active,command,arguments,timespec,times_activated,times_started,last_action,last_started,process_state,pkg_name,pkg_version,pkg_build) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $probe_insert->execute(
            $robotid,
            $probe->{name},
            $probe->{pid},
            $probe->{active},
            $probe->{command},
            $probe->{arguments},
            $probe->{timespec},
            $probe->{times_activated},
            $probe->{times_started},
            $probe->{last_action},
            $probe->{last_started},
            $probe->{process_state},
            $probe->{pkg_name},
            $probe->{pkg_version},
            $probe->{pkg_build}
        ) or die "err: ", $DB2->errstr;
        $probe_insert->finish();

        my @rowID = $DB2->selectrow_array('SELECT @@IDENTITY');
        return $rowID[0];
    }

    return undef;
}

sub setPACKAGE {
    my ($self,$pkg,$robotid) = @_;
    my $DB = $self->{ctx};
    my $DB2 = $self->{ctx2};

    my $sth = $DB->prepare("SELECT * FROM list_robots_pkgs WHERE robotid = ? AND name = ?");
    $sth->execute($robotid,$pkg->{name});

    if($sth->rows) {
        my $pkg_update = $DB2->prepare("UPDATE list_robots_pkgs SET robotid=?,name=?,description=?,version=?,build=?,date=?,install_date=? WHERE name = ?");
        $pkg_update->execute(
        $robotid,
            $pkg->{name},
            $pkg->{description},
            $pkg->{version},
            $pkg->{build},
            $pkg->{date},
            $pkg->{install_date},
            $pkg->{name}
        ) or return -1;
        $pkg_update->finish();
    }
    else {
        my $pkg_insert = $DB2->prepare("INSERT INTO list_robots_pkgs (robotid,name,description,version,build,date,install_date) VALUES(?,?,?,?,?,?,?)");
        $pkg_insert->execute(
            $robotid,
            $pkg->{name},
            $pkg->{description},
            $pkg->{version},
            $pkg->{build},
            $pkg->{date},
            $pkg->{install_date}
        ) or return -1;
        $pkg_insert->finish();
    }

}

1;
