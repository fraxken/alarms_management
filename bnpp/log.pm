package bnpp::log;
use strict;
use warnings;
use Nimbus::API;
use Nimbus::Session;

use Term::ANSIColor qw(:constants);
use Win32::Console::ANSI;
use Data::Dumper;
$Data::Dumper::Indent = 1;

our $level = 3;
our $logfile = 1;
our $GBL_STR_raise_alarm_on_error;
our $GBL_STR_raise_alarm_on_warning;
our %WarnState = (
	1 => "[ERR]     ",
	2 => "[WARNING] ",
	3 => "[INFO]    ",
	4 => "[DEBUG]   ",
	5 => "          "
);


sub dateLog {
	my @months = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
	my @days = qw(Sun Mon Tue Wed Thu Fri Sat Sun);
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	return "$months[$mon] $mday $hour:$min:$sec";
}

sub print {
	my ($logfile,$logmsg,$loglevel) = @_;
	if(not defined($loglevel)) {
		$loglevel = 3;
	}
	my $FH;
    open ($FH,">>","$logfile");
	my $date = dateLog();
	if($loglevel <= $level || $loglevel == 5 || $loglevel == 4) {
		print $FH "$date $WarnState{$loglevel} - $logmsg\n";
	}
    close $FH;
}

sub console {
	my ($msg,$loglevel) = @_;
	if(not defined($loglevel)) {
		$loglevel = 3;
	}
	my $date = dateLog();
	if($loglevel <= $level || $loglevel == 5 || $loglevel == 4){
		print "$date $WarnState{$loglevel} - $msg \n".RESET;
	}

}


1;
