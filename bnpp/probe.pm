use strict;
use warnings;

# Namespace
package bnpp::probe;

# Nimsoft librairies !
use lib "D:/apps/Nimsoft/perllib";
use lib "D:/apps/Nimsoft/Perl64/lib/Win32API";
use Nimbus::API;
use Nimbus::PDS;
use Nimbus::CFG;

use Term::ANSIColor qw(:constants);
use Win32::Console::ANSI;

use Data::Dumper;
$Data::Dumper::Indent = 1;

my %PCM = (
    cdm => {
        custom => {
            pattern => 'disk,custom'
        }
    },
    processes => {
        watcher => {
            pattern => 'watchers'
        }
    },
    logmon => {
        profiles => {
            pattern => 'profiles'
        }
    },
    ntservices => {
        services => {
            pattern => 'services',
            needed_key => {
                active => 'yes'
            }
        }
    },
    ntevl => {
        watcher => {
            pattern => 'watchers'
        }
    },
    dirscan => {
        watcher => {
            pattern => 'watchers'
        }
    }
);

sub new {
    my ($class,$name,$o,$addr) = @_;
    my $this = {
        name            => $o->{$name}{"name"},
        addr            => $addr,
        description     => $o->{$name}{"description"}  || "",
        group           => lc $o->{$name}{"group"}  || "",
        active          => $o->{$name}{"active"},
        type            => $o->{$name}{"type"},
        command         => $o->{$name}{"command"},
        config          => lc $o->{$name}{"config"},
        logfile         => lc $o->{$name}{"logfile"},
        workdir         => lc $o->{$name}{"workdir"},
        arguments       => $o->{$name}{"arguments"} || "",
        pid             => $o->{$name}{"pid"},
        times_started   => $o->{$name}{"times_started"},
        last_started    => $o->{$name}{"last_started"} || 0,
        pkg_name        => $o->{$name}{"pkg_name"} || "",
        pkg_version     => $o->{$name}{"pkg_version"} || "",
        pkg_build       => $o->{$name}{"pkg_build"} || "",
        process_state   => $o->{$name}{"process_state"} || "unknown",
        port            => $o->{$name}{"port"},
        times_activated => $o->{$name}{"times_activated"},
        timespec        => $o->{$name}{"timespec"},
        last_action     => $o->{$name}{"last_action"},
        is_marketplace  => $o->{$name}{"is_marketplace"},
        local_cfg       => undef
    };
    return bless($this,ref($class) || $class);
}

sub Get_CFG {
    my ($self,$filepath) = @_;
    my $tempGroup   = "probes/$self->{group}/$self->{name}/";
    my $configName  = $self->{config};

    $self->{local_cfg} = "$filepath"."$self->{config}";

    if($self->{name} eq "hub") {
        $tempGroup = "hub";
        $configName = "hub.cfg";
    }

    if($self->{name} eq "controller") {
        $tempGroup = "robot";
        $configName = "controller.cfg";
    }

    if($self->{name} eq "nas") {
        $tempGroup = "probes/service/$self->{name}/";
    }

    if($self->{name} eq "controller") {
        my $PDS_args = pdsCreate();
        pdsPut_PCH ($PDS_args,"directory",$tempGroup);
        pdsPut_PCH ($PDS_args,"file","robot.cfg");
        pdsPut_INT ($PDS_args,"buffer_size",10000000);

        my ($RC, $ProbePDS_CFG) = nimRequest("$self->{robotname}",48000, "text_file_get", $PDS_args,3);
        pdsDelete($PDS_args);

        if($RC == NIME_OK) {
            my $CFG_Handler;

            unless(open($CFG_Handler,">>","$filepath/robot.cfg")) {
                warn "\nUnable to create configuration file for robot probe on path $filepath\n";
                return 0;
            }
            my @ARR_CFG_Config = Nimbus::PDS->new($ProbePDS_CFG)->asHash();
            print $CFG_Handler $ARR_CFG_Config[0]{'file_content'};
            close $CFG_Handler;
        }
        else {

        }
    }

    {
        my $PDS_args = pdsCreate();
        pdsPut_PCH ($PDS_args,"directory",$tempGroup);
        pdsPut_PCH ($PDS_args,"file","$configName");
        pdsPut_INT ($PDS_args,"buffer_size",10000000);

        my ($RC, $ProbePDS_CFG) = nimRequest("$self->{robotname}",48000, "text_file_get", $PDS_args,3);
        pdsDelete($PDS_args);

        if($RC == NIME_OK) {
            my $CFG_Handler;

            unless(open($CFG_Handler,">>","$filepath/$configName")) {
                warn "\nUnable to create configuration file for $self->{name} probe on path $filepath\n";
                return 0;
            }
            my @ARR_CFG_Config = Nimbus::PDS->new($ProbePDS_CFG)->asHash();
            print $CFG_Handler $ARR_CFG_Config[0]{'file_content'};
            close $CFG_Handler;

            return 1;
        }
        else {

            return 0;
        }
    }

}

sub Get_LOG {
    my ($self,$filepath) = @_;

    my $logName = "$self->{name}.log";
    my $log_pds = pdsCreate();
    pdsPut_PCH ($log_pds,"directory","probes/$self->{group}/$self->{name}/");
    pdsPut_PCH ($log_pds,"file","$logName");
    pdsPut_INT ($log_pds,"buffer_size",10000000);

    my ($RC_LOG, $LOGPDS) = nimRequest("$self->{robotname}","48000", "text_file_get", $log_pds,3);
    pdsDelete($log_pds);

    if($RC_LOG == NIME_OK) {
        my $CFG_Handler;
        unless(open($CFG_Handler,">>","$filepath/$logName")) {
            warn "\nUnable to create log file\n";
            return 0;
        }
        my @ARR_CFG_Config = Nimbus::PDS->new($LOGPDS)->asHash();
        print $CFG_Handler $ARR_CFG_Config[0]{'file_content'};
        close $CFG_Handler;
        return 1;
    }
    return 0;
}

sub parseCONF {
    my ($self,$DBFull,$ProbeFK) = @_;
    my $monitored = $self->{name};

    if($PCM{$monitored}) {
        my $tempCFG = Nimbus::CFG->new("$self->{local_cfg}");
        foreach my $monitoredSection ( keys %{ $PCM{$monitored} } ) {
            my @array = split(",",$PCM{$monitored}{$monitoredSection}{pattern});
            my $final;
            if(scalar @array > 1) {
                for my $i (0 .. $#array) {
                    if($i == 0) {
                        $final = $tempCFG->{$array[$i]};
                    }
                    else {
                        $final = $final->{$array[$i]};
                    }
                }
            }
            else {
                $final = $tempCFG->{$PCM{$monitored}{$monitoredSection}{pattern}};
            }
            undef $tempCFG;
            undef @array;

            foreach my $key ( keys %{ $final } ) {
                my $Authorize_Insert = 1;
                if($PCM{$monitored}{$monitoredSection}{needed_key}) {
                    foreach my $CONF_STR_Profile ( keys $PCM{$monitored}{$monitoredSection}{needed_key} ) {
                        if($final->{$key} ne '') {
                            if($PCM{$monitored}{$monitoredSection}{needed_key}{$CONF_STR_Profile} ne $final->{$key}{active}) {
                                $Authorize_Insert = 0;
                            }
                        }
                        else {
                            $Authorize_Insert = 0;
                        }
                    }
                }

                if($Authorize_Insert) {
                    my $ssrID = 0;
                    if($final->{$key}->{"mcs_profileid"}) {
                        $ssrID = $final->{$key}->{"mcs_profileid"};
                    }
                    if(lc $monitored eq "cdm") {
                        $key =~ s/#/\//g;
                    }
                    elsif(lc $monitored eq "logmon") {
                        $key =~ s/\//#/g;
                    }

                    my $DB      = $DBFull->{ctx};
                    my $DB2     = $DBFull->{ctx};
                    my $DB3     = $DBFull->{ctx};

                    my $sth = $DB->prepare("SELECT * FROM list_probes_config WHERE probeid = ? AND profile = ?");
                    $sth->execute($ProbeFK,$key);

                    if($sth->rows) {
                        my $update_conf = $DB2->prepare("UPDATE list_probes_config SET probeid=?,profile=?,ssrid=? WHERE probeid = ? AND profile = ?");
                        $update_conf->execute($ProbeFK,$key,$ssrID,$ProbeFK,$key);
                        $update_conf->finish();
                    }
                    else {
                        my $insert_conf = $DB2->prepare("INSERT INTO list_probes_config (probeid,profile,ssrid) VALUES(?,?,?)");
                        $insert_conf->execute($ProbeFK,$key,$ssrID);
                        $insert_conf->finish();
                    }

                }
            }
        }

    }
    return 1;
}

1;
