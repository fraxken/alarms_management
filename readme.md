# Releases notes

## Version 1.0

This is the first version of the script. This script work with BNPP Perl framework created by GENTILHOMME Thomas.

## Version 1.1

> Release date : 30/08/2016, Author : Thomas

- Use field alarm "probe" and not field "suppressionKey" to exclude logmon probe.
- Add a retry mechanism for failed acknowledge http request.

## Version 1.2

> Release date : 01/09/2016, Author : Thomas

- Better audit of the script action.
- Fix multiple errors on date difference (nimDate.pm).

```perl
# Before
my $format = '%d %H:%M:%S %Y';

# After
my $format = '%Y:%m:%d %H:%M:%S';
```

- Save logs into output directory.
- Automatic clean of old log directories (cache_day variable in configuration file). Cache_day default value is '3'.

## Version 1.3

> Release date : 22/09/2016, Author : Thomas

- Better audit trace (for debugging usertag2 & origin).
- Fix bad origin encoding (due to blessed format in the alarms.pm class).
- Add support for multiple origin checking (when a alarm have multiple origin field, now the script check all field).
- Fix bad hostname matching in AssetDB (due to hostname prefix like .fr.net.intra).
- Add filter to exclude undefined hostname and localhost hostname.
- Exclude APIPA hostname (127.0.0.1 && 169.254.x.x).
- Add clear_not_in_assetdb to the alarms configuration option. (To clear or not hostname not in assetdb).
- Add clear_ip to the alarms configuration option. (To clear or not hostname with ip)
- Exclude ip hostname (net_connect probe).
- Exclude "ERROR" hostname from the pool.
- Ignore ibmvm,vmware and hubmon probe.
- Exclude "ENRICHMENT FAILED" usertag2 from acknowledge pool.
- Add fallback value for alarms options.

## Author

- GENTILHOMME Thomas (gentilhomme.thomas@gmail.com)
